# bioanalytics_api
**Introduction:**  
REST API to upload patient images and download results.

**Installation:**  
Like any other django module:  
`python manage.py makemigrations pictures`  
`python manage.py migrate`  
Create users:  
`python manage.py createsuperuser`  
Start the django server:  
`python manage.py runserver`  

**Usage:**  
Either directly from the webrowser try any of the following available APIs:  
_http://127.0.0.1:8000/pictures/pictures_  
List of all available records under currently login user  
_http://127.0.0.1:8000/pictures/pictures/<pk>_  
Detailed info about particular record, record can be also deleted using  
delete method called on this API.  
_http://127.0.0.1:8000/pictures/images_  
List of all images uploaded under currently login user.  
_http://127.0.0.1:8000/pictures/images/<pk>_  
Show the particular image.  
_http://127.0.0.1:8000/pictures/proc_images_  
List of all processed images  available for currently login user.  
_http://127.0.0.1:8000/pictures/images/<pk>_  
Show the particular processed image.  

or using client class in the _client/image_handler.py_ module, where example  
of usage is available in the "__main__" section.

 **Please note, that during first execution there is no any processed image  
 recorded and therefore code in pictures/models.py/some_action need to be  
 commented. Otherwise it will end up with error. Same needs to be done with 
 the last line in the client/image_handler.py**



