import base64
import os.path
import requests


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class InputError(Error):
    """Exception raised for errors in the input data.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message


class UnexpectedError(Error):
    """Exception raised for unexpected errors.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message


def decode_image():
    with open('/home/kamzik/response', mode='rb') as file:
        data = file.read()
        img = base64.decodebytes(data)
        print(type(img))
    with open('image.jpg', "wb") as img_file:
        print(img, file=img_file)


class ImageClient:
    """
    Class for manipulation with images on client side and communication with
    the server.

    Attributes:
        debug -- for debugging outputs enabled.
    """
    def __init__(self, debug=False):
        self.picture_dict = None
        self.connection = None
        self.debug = debug

    @staticmethod
    def verify_dict_content(verified_dict, must_have_list):
        """
        Method for verification input dictionary input. Raise InputError
        exception in a case of failure.
        :param verified_dict: dict to be verified for items.
        :param must_have_list: items that must exist in the dict.
        :return: No value is returned, just exception is raised in a case of
        failure.
        """
        for item in must_have_list:
            if item not in verified_dict:
                raise InputError('Cannot find "{}" in a dictionary provided.'.
                                 format(item))

    def check_connection(self):
        """
        Method for checking the connection with the server.

        :return: No value is returned, just exception is raised in a case of
        failure.
        """
        try:
            must_have_list = ['address',
                              'proc_image_address',
                              'user',
                              'password']
            self.verify_dict_content(self.picture_dict, must_have_list)
            address_list = [self.picture_dict['address'],
                            self.picture_dict['proc_image_address']]
            for address in address_list:
                self.connection = requests.get(address,
                                               auth=(self.picture_dict['user'],
                                                     self.picture_dict['password']))
                if self.connection.status_code != 200:
                    raise ConnectionError('Connection to {} was unsuccessful '
                                          'with the reason code {} from the '
                                          'server.'.format(address,
                                                           self.connection.status_code))
                elif self.debug:
                    print("Connection successful with the following json "
                          "output:\n{}".format(self.connection.json()))
        except InputError:
            raise
        except Exception as e:
            raise ConnectionError('Unable to establish connection to the '
                                  'server at address {} due to {}'.
                                  format(address, e))

    def encode_images(self):
        """
        Method to encode images to base64 encoding (for json).

        :return: list of encoded images.
        """
        images = []
        for path in self.picture_dict['images']:
            if os.path.isfile(path):
                try:
                    with open(path, mode='rb') as file:
                        img = file.read()
                    images.append(base64.encodebytes(img).decode('utf-8'))
                except Exception as e:
                    raise InputError("Encoding the image on path: {} has "
                                     "failed due to: {}".format(path, e))
            else:
                raise InputError("Provided path: {} to image is not valid.".
                                 format(path))
        return images

    def encode_images_and_push(self, picture_dict):
        """
        Encode images in the provided record in the dict and push them to the
        server.
        :param picture_dict: Record dictionary with all required values filled
        in including image paths.
        :return: No value is returned, just exception is raised in a case of
        failure.
        """
        self.picture_dict = picture_dict
        try:
            must_have_list = ['patient_id', 'exam_id', 'type_of_exam_id',
                              'images']
            self.verify_dict_content(self.picture_dict, must_have_list)
            images = self.encode_images()
            data = {
                'patient_id': self.picture_dict['patient_id'],
                'exam_id': self.picture_dict['exam_id'],
                'type_of_exam_id': self.picture_dict['type_of_exam_id'],
                'images': [{"image": image} for image in images],
            }
            self.connection = requests.post(self.picture_dict['address'],
                                            auth=(self.picture_dict['user'],
                                                  self.picture_dict['password']),
                                            json=data)
            if self.connection.status_code != 201:
                raise ConnectionError('Connection to {} was unsuccessful with '
                                      'the reason code {} from the server.'.
                                      format(self.picture_dict['address'],
                                             self.connection.status_code))
            elif self.debug:
                print("Connection successful with the following json output:\n"
                      "{}".format(self.connection.json()))
            return self.connection.json()['pk']
        except Exception as e:
            self.check_connection()
            raise UnexpectedError("The following unexpected failure has been "
                                  "captured: {}, during a creation of the "
                                  "record".format(e))

    def get_patient_data(self, pk):
        """
        Method to gather patient record data from the server.
        :param pk: primary key of the patient record.
        :return: Json dictionary with the patient data is returned.
        """
        try:
            self.connection = requests.get(self.picture_dict['address']
                                           + str(pk) + '/',
                                           auth=(self.picture_dict['user'],
                                                 self.picture_dict['password']))
            if self.connection.status_code != 200:
                raise ConnectionError('Connection to {} was unsuccessful with '
                                      'the reason code {} from the server.'.
                                      format(self.picture_dict['address'],
                                             self.connection.status_code))
            else:
                return self.connection.json()
        except Exception as e:
            self.check_connection()
            raise UnexpectedError("The following unexpected failure has been "
                                  "captured: {}, during a creation of the "
                                  "record".format(e))

    def get_processed_images(self, image_list, download_folder):
        """
        Get the processed images from the pk list in the patient data
        :param image_list: list of pk to be downloaded
        :param download_folder: folder to download particular processed images.
        :return: No value is returned, just exception is raised in a case of
        failure.
        """
        if not os.path.isdir(download_folder):
            os.mkdir(download_folder)
        try:
            for image_pk in image_list:
                print(image_pk)
                self.connection = requests.get(self.picture_dict['proc_image_address']
                                               + str(image_pk) + '/',
                                               auth=(self.picture_dict['user'],
                                                     self.picture_dict['password']))
                if self.connection.status_code != 200:
                    raise ConnectionError('Connection to {} was unsuccessful '
                                          'with the reason code {} from the '
                                          'server.'.
                                          format(self.picture_dict['proc_image_address'],
                                                 self.connection.status_code))
                else:
                    content_type = self.connection.headers['content-type'].split('/')
                    if content_type[0] == 'image':
                        filename = '{}.{}'.format(str(image_pk), content_type[-1])
                        with open(download_folder+filename, 'wb') as picture:
                            picture.write(self.connection.content)
                    else:
                        raise UnexpectedError("Unexpected content type: '{}' "
                                              "received.".
                                              format(content_type[0]))
            if self.debug:
                print("All images downloaded successfully.")
        except Exception as e:
            self.check_connection()
            raise UnexpectedError("The following unexpected failure has been "
                                  "captured: {}, during a creation of the "
                                  "record".format(e))


if __name__ == "__main__":
    # Create an image instance:
    image_instance = ImageClient(debug=True)
    # Create the dictionary with all the necessary parameters for the record
    patient_dict = {
        "address": "http://127.0.0.1:8000/pictures/pictures/",
        "proc_image_address": "http://127.0.0.1:8000/pictures/proc_images/",
        "user": "root",
        "password": "toor",
        "patient_id": "333",
        "exam_id": "333",
        "type_of_exam_id": "333",
        "images": ["/home/kamzik/Obrázky/HjMsO.png",
                   "/home/kamzik/Obrázky/njdscb.jpg"]
    }
    # Encode images and push the record on the server
    instance_id = image_instance.encode_images_and_push(patient_dict)
    # Get the data for the record
    json_data = image_instance.get_patient_data('1')
    # Download processed images from the record data into required folder
    image_instance.get_processed_images(json_data['processed_images'], 'media/')

