from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from pictures import views

urlpatterns = [
    path('pictures/', views.PictureList.as_view()),
    path('pictures/<int:pk>/', views.PictureDetail.as_view()),
    # For debugging purposes only. Can be uncommented if found useful in the
    # future.
    # path('users/', views.UserList.as_view()),
    # path('users/<int:pk>/', views.UserDetail.as_view()),
    path('images/', views.ImageList.as_view()),
    path('images/<int:pk>/', views.ImageDetail.as_view()),
    path('proc_images/', views.ProcessedImageList.as_view()),
    path('proc_images/<int:pk>/', views.ProcessedImageDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)