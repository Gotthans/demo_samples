from pictures.models import Picture
from pictures.models import ImageAlbum
from pictures.models import ProcessedImageAlbum
from django.contrib.auth.models import User
from pictures.serializers import PictureSerializer
from pictures.serializers import ImageSerializer
from pictures.serializers import ProcessedImageSerializer
from pictures.serializers import PictureBackSerializer
from pictures.serializers import UserSerializer
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions


class PictureList(APIView):
    """
    List all pictures, or create a new picture.
    """
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        pictures = Picture.objects.all().filter(owner=self.request.user)
        serializer = PictureBackSerializer(pictures, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PictureSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PictureDetail(APIView):
    """
    Retrieve, update or delete a picture instance.
    """

    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, pk):
        try:
            object = Picture.objects.get(pk=pk)
            if object.owner == self.request.user:
                return object
            else:
                raise PermissionDenied()
        except Picture.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        picture = self.get_object(pk)
        serializer = PictureBackSerializer(picture)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        picture = self.get_object(pk)
        picture.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ImageList(APIView):
    """
    List all images content
    """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        images = ImageAlbum.objects.filter(picture__owner=self.request.user)
        serializer = ImageSerializer(images, many=True)
        return Response(serializer.data)


class ImageDetail(APIView):
    """
    Retrieve a image instance.
    """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        image = ImageAlbum.objects.get(pk=pk,
                                       picture__owner=self.request.user)
        extension = str(image.image).split('.')[-1]
        return HttpResponse(image.image,
                            content_type='image/{}'.format(extension))


class ProcessedImageList(APIView):
    """
    List all processed images content
    """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        images = ProcessedImageAlbum.objects.filter(picture__owner=self.request.user)
        serializer = ProcessedImageSerializer(images, many=True)
        return Response(serializer.data)


class ProcessedImageDetail(APIView):
    """
    Retrieve a image instance.
    """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        image = ProcessedImageAlbum.objects.get(pk=pk,
                                                picture__owner=self.request.user)
        extension = str(image.image).split('.')[-1]
        return HttpResponse(image.image,
                            content_type='image/{}'.format(extension))


# For debugging purposes only, the related code is commented under urls.py
class UserList(APIView):
    """
    List all users content
    """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


# For debugging purposes only, the related code is commented under urls.py
class UserDetail(APIView):
    """
    Retrieve or delete a user instance.
    """

    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, pk):
        try:
            object = User.objects.get(pk=pk)
            if object.username == self.request.user:
                return object
            else:
                raise PermissionDenied()
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = User.objects.get(pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
