import os
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


def upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    milliseconds = now.microsecond // 1000
    return f"media/upload/{now:%Y%m%d%H%M%S}{milliseconds}{extension}"


def some_action():
    print("Trigger some action here before you save a model.")
    # The following lines are for testing purposes only
    # It basically uploads one of the media pictures into 1st record
    # processed_images field from where it can be downloaded.
    picture_instance = Picture.objects.get(pk=1)
    # 'media/upload/20211031214518591.png'
    ProcessedImageAlbum.objects.create(image='media/upload/20211031214518591.png',
                                       picture=picture_instance)


class Picture(models.Model):
    owner = models.ForeignKey(User,
                              related_name='pictures',
                              on_delete=models.CASCADE)
    patient_id = models.IntegerField()
    exam_id = models.IntegerField()
    type_of_exam_id = models.IntegerField()
    text_results = models.TextField()
    exam_id_feedback = models.IntegerField(default=0, blank=True, null=True)

    def save(self, *args, **kwargs):
        some_action()
        super(Picture, self).save(*args, **kwargs)

    class Meta:
        ordering = ['pk']


class ImageAlbum(models.Model):
    picture = models.ForeignKey(Picture,
                                related_name='images',
                                on_delete=models.CASCADE)
    image = models.ImageField(upload_to=upload_to)


class ProcessedImageAlbum(models.Model):
    picture = models.ForeignKey(Picture,
                                related_name='processed_images',
                                on_delete=models.CASCADE)
    image = models.ImageField(upload_to=upload_to)
