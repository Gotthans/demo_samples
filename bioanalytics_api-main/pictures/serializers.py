from rest_framework import serializers
from pictures.models import Picture
from pictures.models import ImageAlbum
from pictures.models import ProcessedImageAlbum
from django.contrib.auth.models import User


class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        if isinstance(data, six.string_types):
            if 'data:' in data and ';base64,' in data:
                header, data = data.split(';base64,')

            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            file_name = str(uuid.uuid4())[:32] # 32 characters are more than enough.
            file_extension = self.get_file_extension(file_name, decoded_file)
            complete_file_name = "%s.%s" % (file_name, file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)
        return super(Base64ImageField, self).to_internal_value(data)

    @staticmethod
    def get_file_extension(file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class ImageSerializer (serializers.ModelSerializer):
    image = Base64ImageField(max_length=None, use_url=True)

    class Meta:
        model = ImageAlbum
        fields = ('id', 'image')


class ProcessedImageSerializer (serializers.ModelSerializer):
    image = Base64ImageField(max_length=None, use_url=True)

    class Meta:
        model = ProcessedImageAlbum
        fields = ('id', 'image')


class PictureSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    images = ImageSerializer(many=True, required=False)

    class Meta:
        model = Picture
        fields = ['pk', 'owner', 'patient_id', 'exam_id', 'type_of_exam_id',
                  'images']

    def create(self, validated_data):
        if 'images' in validated_data:
            image_album = validated_data.pop('images')
            picture_instance = Picture.objects.create(**validated_data)
            for img in image_album:
                ImageAlbum.objects.create(**img, picture=picture_instance)
            return picture_instance
        if 'images' not in validated_data:
            picture_instance = Picture.objects.create(**validated_data)
            return picture_instance


class PictureBackSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Picture
        fields = ['pk', 'owner', 'patient_id', 'exam_id', 'type_of_exam_id',
                  'images', 'processed_images', 'text_results',
                  'exam_id_feedback']


# For debugging purposes only, the related code is commented under urls.py
class UserSerializer(serializers.ModelSerializer):
    pictures = serializers.PrimaryKeyRelatedField(many=True,
                                                  queryset=Picture.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'pictures']
