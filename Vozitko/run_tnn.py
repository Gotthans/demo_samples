from PIL import Image
import global_variable
from keras.models import Model
from keras.layers import *

from tiramisu.camvid.mapping import decode
from tiramisu.tiramisu.model import create_tiramisu
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import time
from sql_connection import DB_operations


               
def color_label(img, id2code):
    rows, cols = img.shape
    result = np.zeros((rows, cols, 3), 'uint8')
    for j in range(rows):
        for k in range(cols):
            result[j, k] = id2code[img[j, k]]
    return result


class convolutional_neural_network():
    def __init__(self):
        # Load model and weights
        print("Loading NN models and weights")
        self.path_to_model='/home/nvidia/Documents/Drivers/tiramisu/models/CEDA5.h5'
        self.path_to_labels_list='/home/nvidia/Documents/Drivers/tiramisu/models/CEDA_label_colors.txt'
        self.input_shape = (224, 224, 3)
        self.number_classes = 14  # CamVid data consist of 32 classes
    
        self.img_input = Input(shape=self.input_shape)
        self.x = create_tiramisu(self.number_classes, self.img_input)
        self.model = Model(self.img_input, self.x)
        self.model.load_weights(self.path_to_model)
        global_variable.nn_status = "NN ready"
        print("Convolutional Neural Network initialized")

        #test image
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        test_path = ROOT_DIR+'/images/test_image058.png'
        # load your own image
    
        try_image = Image.open(test_path).resize((self.input_shape[0], self.input_shape[1]), Image.NEAREST)
        #try_image.show()

        try_image = np.array(try_image)
        try_image = try_image / 255.
        try_image -= try_image.mean()
        try_image /= try_image.std()


        # Use loaded model for prediction on input image
        prediction = self.model.predict(np.expand_dims(try_image, 0))
        prediction = np.argmax(prediction, axis=-1)

        # Visualize the outcome
        outcome = np.resize(prediction, (self.input_shape[0], self.input_shape[1]))
        label_codes, label_names, code2id = decode(self.path_to_labels_list)
        #print(list(zip(label_codes, label_names)))

        id2code = {val: key for (key, val) in code2id.items()}
        outcome = color_label(outcome, id2code)
        img = Image.fromarray(outcome)
        print("Neural Network is ready")
        
    
    def classification_of_image(self):

        print("Neural Network on Image is being evaluated")

        while global_variable.Global_RUN == 1:
            try:
                ID_project = global_variable.id_project
                path = '/media/nvidia/ssd1/Webcam/'+str(ID_project)
                files =""
    
                files = glob.glob(path+'/*.png')
                files.sort(key=os.path.getmtime)


                # load your own image
            
                try_image = Image.open(files[-2]).resize((self.input_shape[0], self.input_shape[1]), Image.NEAREST)
                #try_image.show()

                try_image = np.array(try_image)
                try_image = try_image / 255.
                try_image -= try_image.mean()
                try_image /= try_image.std()


                # Use loaded model for prediction on input image
                prediction = self.model.predict(np.expand_dims(try_image, 0))
                prediction = np.argmax(prediction, axis=-1)

                # Visualize the outcome
                outcome = np.resize(prediction, (self.input_shape[0], self.input_shape[1]))
                label_codes, label_names, code2id = decode(self.path_to_labels_list)
                #print(list(zip(label_codes, label_names)))

                id2code = {val: key for (key, val) in code2id.items()}
                outcome = color_label(outcome, id2code)


                img = Image.fromarray(outcome)
                #analysis
                vyrez = np.array(img)
                vyrez = vyrez[-80:-20][:]
                #print(vyrez.shape)
                vyrez2= vyrez[30:38,:,:]
                #print(vyrez2.shape)'/media/nvidia/ssd1/Webcam/'+str(ID_project)
                result = Image.fromarray(vyrez2)
                #result.show()
                x = 1280
                y = 1024
                paper = 4.2 #calibrated value with meter
                A4x = 29.7
                line = 20.25
                upscale = x/img.size[0]
                cmPixels = (x/line) #63.05
                factor = A4x/(paper* cmPixels)
                #print((224*upscale)*factor)

                #find left
                i = 0
                distance = 0
                
                for position in range(30,31):
                    indicator = 0
                    
                    for i in range(vyrez.shape[1]):
                        if vyrez[position,i,0]==236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                            indicator = indicator+1
                            if indicator>2:
                                break
                    left = i
                    indicator = 0
                    i = 0
                    for i in reversed(range(vyrez.shape[1])):
                        if vyrez[position,i,0]==236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                            indicator = indicator+1
                            if indicator>2:
                                break
                    right = i
                    distance = ((right-left)*upscale)*factor
                distance = int(distance)

                if distance > 137:
                    distance = 140
                if distance < 0: 
                    distance = 0

                global_variable.nn_status = "NN operational"
                support_text_conversion = files[-2][:-4]
                support_text_conversion = support_text_conversion.replace('/media/nvidia/ssd1/Webcam/'+str(ID_project)+'/',"")
                global_variable.nn_distance = str(distance)

                #save result into db
                sql_update = DB_operations()
                sql_update.update("UPDATE `CEDA`.`data` SET `ID_project`="+ str(ID_project) +", `width_of_sidewalk`="+ str(distance)+" WHERE `nnet_photo`='"+support_text_conversion+"'")
                time.sleep(2)
            
            except:
                global_variable.nn_status ="NN Error"
    def post_analysis(self):
        print("Neural Network on Image is being evaluated")
        path = '/media/nvidia/ssd1/Webcam/1'
        files =""

        files = glob.glob(path+'/*.png')
        files.sort(key=os.path.getmtime)


        # load your own image
        for row in files:

            try_image = Image.open(row).resize((self.input_shape[0], self.input_shape[1]), Image.NEAREST)
            #try_image.show()

            try_image = np.array(try_image)
            try_image = try_image / 255.
            try_image -= try_image.mean()
            try_image /= try_image.std()


            # Use loaded model for prediction on input image
            prediction = self.model.predict(np.expand_dims(try_image, 0))
            prediction = np.argmax(prediction, axis=-1)

            # Visualize the outcome
            outcome = np.resize(prediction, (self.input_shape[0], self.input_shape[1]))
            label_codes, label_names, code2id = decode(self.path_to_labels_list)
            #print(list(zip(label_codes, label_names)))

            id2code = {val: key for (key, val) in code2id.items()}
            outcome = color_label(outcome, id2code)


            img = Image.fromarray(outcome)
            row2 = row.replace('/media/nvidia/ssd1/Webcam/1/',"")
            img.save("/media/nvidia/ssd1/Webcam/Outcome/"+row2)
            #img.show()
            #analysis
            vyrez = np.array(img)
            vyrez = vyrez[-80:-20][:]
            #print(vyrez.shape)
            vyrez2= vyrez[30:38,:,:]
            #print(vyrez2.shape)'/media/nvidia/ssd1/Webcam/'+str(ID_project)
            result = Image.fromarray(vyrez2)
            #result.show()
            x = 1280
            y = 1024
            paper = 4.2 #calibrated value with meter
            A4x = 29.7
            line = 20.25
            upscale = x/img.size[0]
            cmPixels = (x/line) #63.05
            factor = A4x/(paper* cmPixels)
            #print((224*upscale)*factor)

            #find left
            i = 0
            distance = 0
            
            for position in range(30,31):
                indicator = 0
                
                for i in range(vyrez.shape[1]):
                    if vyrez[position,i,0]==236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                        indicator = indicator+1
                        if indicator>2:
                            break
                left = i
                indicator = 0
                i = 0
                for i in reversed(range(vyrez.shape[1])):
                    if vyrez[position,i,0]==236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                        indicator = indicator+1
                        if indicator>2:
                            break
                right = i
                distance = ((right-left)*upscale)*factor
            distance = int(distance)

            if distance > 137:
                distance = 140
            if distance < 0: 
                distance = 0

            support_text_conversion = row[:-4]
            support_text_conversion = support_text_conversion.replace('/media/nvidia/ssd1/Webcam/1/',"")
            print(str(distance))

            #save result into db
            #sql_update = DB_operations()
            #sql_update.update("UPDATE `CEDA`.`data` SET `ID_project`=1, `width_of_sidewalk`="+ str(distance)+" WHERE `nnet_photo`='"+support_text_conversion+"'")

    def example_analysis(self):
        print("Neural Network on Image is being evaluated")
        path = '/media/nvidia/ssd1/Webcam/1'
        files =""

        files = glob.glob(path+'/*.png')
        files.sort(key=os.path.getmtime)

        for row in files:


            try_image = Image.open(row).resize((self.input_shape[0], self.input_shape[1]), Image.NEAREST)
            try_image.show()

            try_image = np.array(try_image)
            try_image = try_image / 255.
            try_image -= try_image.mean()
            try_image /= try_image.std()


            # Use loaded model for prediction on input image
            prediction = self.model.predict(np.expand_dims(try_image, 0))
            prediction = np.argmax(prediction, axis=-1)

            # Visualize the outcome
            outcome = np.resize(prediction, (self.input_shape[0], self.input_shape[1]))
            label_codes, label_names, code2id = decode(self.path_to_labels_list)
            #print(list(zip(label_codes, label_names)))

            id2code = {val: key for (key, val) in code2id.items()}
            outcome = color_label(outcome, id2code)


            img = Image.fromarray(outcome)
            row2 = row.replace('/media/nvidia/ssd1/Webcam/1/',"")
            img.save("/media/nvidia/ssd1/Webcam/Outcome/"+row2)
            img.show()

            #analysis
            vyrez = np.array(img)
            vyrez = vyrez[-80:-20][:]
            #print(vyrez.shape)
            vyrez2= vyrez[30:38,:,:]
            #print(vyrez2.shape)'/media/nvidia/ssd1/Webcam/'+str(ID_project)
            result = Image.fromarray(vyrez2)
            #result.show()
            x = 1280
            y = 1024
            paper = 4.2 #calibrated value with meter
            A4x = 29.7
            line = 20.25
            upscale = x/img.size[0]
            cmPixels = (x/line) #63.05
            factor = A4x/(paper* cmPixels)
            #print((224*upscale)*factor)

            #find left
            i = 0
            distance = 0
            
            for position in range(30,31):
                indicator = 0
                
                for i in range(vyrez.shape[1]):
                    if vyrez[position,i,0] == 236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                        indicator = indicator+1
                        if indicator > 2:
                            break
                left = i
                indicator = 0
                i = 0
                for i in reversed(range(vyrez.shape[1])):
                    if vyrez[position,i,0] == 236 and vyrez[position,i,1] == 91 and vyrez[position,i,2]:
                        indicator = indicator+1
                        if indicator > 2:
                            break
                right = i
                distance = ((right-left)*upscale)*factor
            distance = int(distance)

            if distance > 137:
                distance = 140
            if distance < 0: 
                distance = 0

            support_text_conversion = row[:-4]
            support_text_conversion = support_text_conversion.replace('/media/nvidia/ssd1/Webcam/1/',"")
            print(str(distance))
            input("Press Enter to continue...")