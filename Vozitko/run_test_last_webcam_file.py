#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 12:57:57 2019

@author: JG
"""
import os
from PIL import Image
import glob

ID_project = 1
path = '/media/nvidia/ssd1/Webcam/'+str(ID_project)

files = glob.glob(path+'/*.png')
files.sort(key=os.path.getmtime)

input_shape = (224, 224, 3)
try_image = Image.open(files[-1]).resize((input_shape[0], input_shape[1]), Image.NEAREST)
try_image.show()